package com.kmj.employeemanagementapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Entity

public class TheStaff {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private String id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String companyDep;

    @Column(nullable = false)
    private String companyPo;

    @Column(nullable = false)
    private LocalDate startDay;

    @Column(nullable = false)
    private LocalDate endDay;

    @Column(nullable = false)
    private String accountNumber;

    @Column(nullable = false)
    private Byte annualDay;

    @Column(nullable = false)
    private Boolean insurance;

    @Column(nullable = false)
    private String phoneNumber;

    @Column(nullable = false)
    private String identityNumber;

    @Column(nullable = false)
    private String homeAddress;
}
